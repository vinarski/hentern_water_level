#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> //for gethostbyname
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sqlite3.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <time.h>

#define LOCATIONS_MAX_NUM 100
#define BUFFER_SIZE 50024
#define TRUE 1
#define FALSE 0
#define PORT 80

typedef struct locations
{
    char database_path_name [100];
    int num_locations;
    int location_id[LOCATIONS_MAX_NUM];
    char location_url[LOCATIONS_MAX_NUM][BUFFER_SIZE];
} locations_t;

locations_t locations;

typedef struct measurement
{
    int value_cm;
    char date_time[25];
} measurement_t;

static long convert_str_to_long (const char *str)
{
    long value;
    char *end;

    if (str[0] == '\0')
    {
        printf("\nconfig file is corrupt\n");
        exit (EXIT_FAILURE);
    }
    //discards any whitespace characters (as identified by calling isspace()
    value = strtol (str, &end, 10);

    /*  test for an error */
    if (value == LONG_MIN || value == LONG_MAX)
    {
        /* If a conversion error occurred, display a message and exit */
        if (errno == EINVAL)
        {
            printf("config file is corrupt - conversion error occurred: %d\n",
                   errno);
            exit (EXIT_FAILURE);
        }

        /* If the value provided was out of range, display a warning message */
        if (errno == ERANGE)
        {
            printf("config file is corrupt - the value provided was out of range\n");
            exit (EXIT_FAILURE);
        }

        printf("config file is corrupt - the value is 0\n");
        exit (EXIT_FAILURE);
    }

    return value;
}

static void read_config_file (void)
{
    static int func_call_num;

    char buffer[BUFFER_SIZE + 1];
    FILE *config_file = NULL;

    func_call_num ++;
    if (func_call_num > 1)
        return;

    config_file = fopen ("/home/pi/coding/hentern_water_level/config", "r");
    if (config_file == NULL)
    {
        printf("\nfailure opening config file\n");
        exit (EXIT_FAILURE);
    }

    int first_line = 1;
    while (TRUE)
    {
        char *ret;
        char *equals_char = "="; //config file separator
        char *equals_char_ptr;
        char *line_value;

	/* Database path and name  */
        if (first_line)
        {
            ret = fgets (buffer, BUFFER_SIZE, config_file);

            if (ret == NULL)
                break;

            equals_char_ptr = strstr (buffer, equals_char);
            if (equals_char_ptr == NULL)
            {
                printf("\nconfig file is corrupt\n");
                exit (EXIT_FAILURE);
            }

            equals_char_ptr ++;
            line_value = equals_char_ptr;
	    int len = strlen (line_value);
            if (!len)
	    {
                printf("\nconfig file is corrupt\n");
                exit (EXIT_FAILURE);
            }
            memcpy (locations.database_path_name, line_value, len);
            //len-1 to delete the '\n' character
            locations.database_path_name[len-1] = 0;
	    first_line = 0;
	}

	/* LOCATION ID LINE */
        //fgets vozvrashyaet kak dopolnitelnyj servis adres stroki arrBuffer
        ret = fgets (buffer, BUFFER_SIZE, config_file);
        //DEBUG printf ("ret addr: %p, buffer addr: %p\n", ret, buffer);
        if (ret == NULL)
            break;

        //DEBUG
	//printf ("\ncontents line : %s\n", buffer);

        equals_char_ptr = strstr (buffer, equals_char);
        if (equals_char_ptr == NULL)
        {
            printf("\nconfig file is corrupt\n");
            exit (EXIT_FAILURE);
        }

        equals_char_ptr ++;
        line_value = equals_char_ptr;

        locations.location_id[locations.num_locations] =
            convert_str_to_long (line_value);

	/* LOCATION URL LINE */
        ret = fgets (buffer, BUFFER_SIZE, config_file);

        if (ret == NULL)
            break;

        //DEBUG
	//printf ("\ncontents line %s\n", buffer);

        equals_char_ptr = strstr (buffer, equals_char);
        if (equals_char_ptr == NULL)
        {
            printf("\nconfig file is corrupt\n");
            exit (EXIT_FAILURE);
        }

        equals_char_ptr ++;
        line_value = equals_char_ptr;
	int l = strlen (line_value);
        if (!l)
		break;

        memcpy (locations.location_url[locations.num_locations++],
                line_value, l);
        //l-1 to delete the '\n' character
        locations.location_url[locations.num_locations++][l-1] = 0;
//DEBUG
//printf ("\nlocations.num_locations: %d\n", locations.num_locations);
    }
    fclose (config_file);
}

static int create_named_server_socket (void)
{
    struct sockaddr_in server_address;
    int server_sock_fd;
    int result;
    int len;
    struct hostent *hst;

    //works either way (name to ip addr, or ip addr to 4 bytes ip addr)
    hst = gethostbyname ("www.hochwasser-rlp.de");
    if (hst == NULL)
    {
        herror ("gethostbyname");
        exit (EXIT_FAILURE);
    }

    if ((server_sock_fd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("remote server socket creation error");
        exit (EXIT_FAILURE);
    }

    memset(&server_address, 0, sizeof (server_address));
    server_address.sin_family = AF_INET;
    memcpy (&server_address.sin_addr, hst->h_addr, hst->h_length);
    server_address.sin_port = htons(PORT);
    len = sizeof (server_address);

    result = connect (server_sock_fd, (struct sockaddr *) &server_address, len);
    if (result == -1)
    {
        perror ("oops: remote server socket setup failed");
        exit (EXIT_FAILURE);
    }

    return server_sock_fd;
}


static measurement_t communication (int server_socket_fd)
{
    int res = 0;
    char buffer[BUFFER_SIZE + 1] =
    "GET /karte/einzelpegel/flussgebiet/mosel/pegel/HENTERN HTTP/1.1\r\nHost: www.hochwasser-rlp.de\r\n\r\n";
    measurement_t measurement;

    res = write (server_socket_fd, buffer, strlen(buffer));
    if (res <= 0)
    {
        close (server_socket_fd);
        printf ("Request failed\n");
        exit (EXIT_FAILURE);
    }

    int total_bytes = 0;
    int payload_bytes = 0;
    int payload_bytes_found = 0;
    int is_break = 0;
    char result[BUFFER_SIZE + 1];
    for (int i = 0; !is_break; i ++)
    {
        res = read (server_socket_fd, buffer, BUFFER_SIZE);

        if (res <= 0)
        {
            printf ("no response from the server\n");
            exit (EXIT_FAILURE);
        }

        buffer[res] = 0;

        if (!payload_bytes_found)
        {
            char *pos_rnrn = strstr (buffer, "\r\n\r\n");
            if (pos_rnrn != NULL)
            {
                payload_bytes = (int)strtol (pos_rnrn + 4, NULL, 16);
                payload_bytes_found = 1;
                total_bytes = res - (pos_rnrn + 4 - buffer);
            }
        }
        else
        {
            total_bytes += res;
        }

        if (payload_bytes_found && total_bytes >= payload_bytes)
            is_break = 1;

        char *last_measure_value = strstr (buffer, "Letzter Messwert:");
        if (last_measure_value != NULL)
        {
            char *last_measure_value_end = strstr (last_measure_value, "</p>");
            if (last_measure_value_end != NULL)
            {
                int result_length = last_measure_value_end - last_measure_value;
                memcpy (result, last_measure_value, result_length);
                result[result_length] = 0;

                char *date = strstr (result, "<strong>") + 8;
                memcpy (measurement.date_time, date, 21);
                measurement.date_time[21] = 0;

                char *cm = strstr (result, "</strong>") - 6;

                measurement.value_cm  = atoi (cm);

                return measurement;
            }
        }

    }
}

static void insert_record (measurement_t measurement)
{
    sqlite3 *db;
    char *err;
    int rc;
    char sql[1000];

    rc = sqlite3_open (locations.database_path_name, &db);
    if (rc)
    {
        printf ("Can't open database: %s\n", sqlite3_errmsg (db));
        sqlite3_close (db);
    }

    /* Row insert */

    char snum[11];

    sql[0] = 0;
    strcat (sql,
    "INSERT INTO records (location_id, value_cm, unixtimestamp, timeAsText) VALUES (0,");
    sprintf(snum, "%d", measurement.value_cm);
    strcat (sql, snum);
    strcat (sql, ",");
    sprintf(snum, "%d", (int)time(NULL));
    strcat (sql, snum);
    strcat (sql, ",'");
    strcat (sql, measurement.date_time);
    strcat (sql, "');");

    rc = sqlite3_exec (db, sql, NULL, NULL, &err);
    if (rc != SQLITE_OK && err != NULL)
    {
        printf ("SQL error: %s\n", err);
        sqlite3_free (err);
    }

    /* Close database */
    sqlite3_close (db);
}

int main (void)
{
    int server_socketfd;
    measurement_t measurement;

    read_config_file ();
    server_socketfd = create_named_server_socket ();
    measurement = communication (server_socketfd);

    insert_record (measurement);

    return 0;
}
