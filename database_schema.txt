-- main tables
CREATE TABLE "records" ("record_id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, "location_id" INTEGER NOT NULL, "value_cm" INTEGER NOT NULL, "unixtimestamp" INTEGER NOT NULL, "timeAsText" TEXT, "comment" TEXT);
CREATE TABLE "locations" ("location_id" INTEGER PRIMARY KEY NOT NULL UNIQUE , "description" TEXT UNIQUE);

-- indices

-- filling db
INSERT INTO locations (location_id, description) VALUES (0, 'Hentern');
